<%-- 
    Document   : cards
    Created on : Oct 30, 2022, 3:52:53 AM
    Author     : cuongseven
--%>

<%@page import="java.sql.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html
    lang="en"
    class="light-style layout-menu-fixed"
    dir="ltr"
    data-theme="theme-default"
    data-assets-path="../assets/"
    data-template="vertical-menu-template-free"
    >
    <head>
        <meta charset="utf-8" />
        <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"
            />

        <title>FPT University Library</title>

        <meta name="description" content="" />

        <!-- Favicon -->
        <link rel="icon" type="image/x-icon" href="../assets/img/favicon/favicon.ico" />

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link
            href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
            rel="stylesheet"
            />

        <!-- Icons. Uncomment required icon fonts -->
        <link rel="stylesheet" href="../assets/vendor/fonts/boxicons.css" />

        <!-- Core CSS -->
        <link rel="stylesheet" href="../assets/vendor/css/core.css" class="template-customizer-core-css" />
        <link rel="stylesheet" href="../assets/vendor/css/theme-default.css" class="template-customizer-theme-css" />
        <link rel="stylesheet" href="../assets/css/demo.css" />

        <!-- Vendors CSS -->
        <link rel="stylesheet" href="../assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css" />

        <!-- Page CSS -->

        <!-- Helpers -->
        <script src="../assets/vendor/js/helpers.js"></script>

        <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
        <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
        <script src="../assets/js/config.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

        <script>
            $(document).ready(function () {
                $("button").click(function () {
                    var onDisableFunc = $("#btnActive").attr('disabled', '');
//                    const timer = setTimeout(onDisableFunc, 3000);
                });
            });
        </script>
    </head>


    <body>
        <!-- Layout wrapper -->
        <div class="layout-wrapper layout-content-navbar">
            <div class="layout-container">
                <!-- Menu -->
                <%@include file="NavigationLeft.jsp" %>

                <!-- / Menu -->

                <!-- Layout container -->
                <div class="layout-page">
                    <!-- Info Course -->
                    <div class="container-xxl flex-grow-1 container-p-y">
                        <div class="col-md">
                            <div class="card mb-3">
                                <div class="row g-0">
                                    <div class="col-md-7">
                                        <div class="card-body">
                                            <h2 class="card-title text-primary fw-semibold">Programming name</h2> <br>
                                            <h5 class="card-title">ABOUT THIS COURSE</h5>
                                            <p class="card-text ">Ultricies aliquet egestas, massa nunc risus cumque et consequatur ater quisquam. Per deserunt innec! Aliqua beatae blanditiis rhoncus. Risus, consectetur justo fugiat? Ipsum officia, iusto harum doloremque, adipisicing ipsa soluta massa corporis atque quam placerat leo. Hymenaeos leo aliquip, donec, aut ornare aperiam laboris, proin? Ipsam posuere modi hendrerit! Excepteur doloribus justo fusce sapien, minima? Ridiculus harum ac aperiam convallis! Necessitatibus praesent numquam maiores.</p>
                                            <p class="card-text ">Ultricies aliquet officia, iusto harum doloremque, adipisicing ipsa soluta massa corporis atque quam placerat leo. Hymenaeos leo aliquip, donec, aut ornare aperiam laboris, proin? Ipsam posuere modi hendrerit! Excepteur doloribus justo fusce sapien, minima? Ridiculus harum ac aperiam convallis! Necessitatibus praesent numquam maiores.</p>
                                            <p class="card-text"><small class="text-uppercase">Create by Toni Teo</small></p>
                                            <p class="card-text"><small class="text-uppercase">Release 20/11/2022</small></p>
                                        </div>
                                        <div class="row mb-lg-5">
                                            <div class="col-md-2">
                                            </div> 
                                            <div class="col-md-4">
                                                <button id="btnActive" type="button" class="btn btn-lg btn-primary">Enroll
                                                    <small class="text-white-50"><br>
                                                        <script>
                                                            document.write(
                                                                    new Date().getDate() + "/" +
                                                                    (new Date().getMonth() + 1) + "/" +
                                                                    new Date().getFullYear());
                                                        </script>
                                                    </small>
                                                </button>
                                            </div>
                                            <div  class="col-md-4">
                                                <a href="/Learn" class="menu-link">
                                                    <button id="btnActive" type="button" class="btn btn-primary" style="height: 75px" onclick="handleBtnDisable()"> 
                                                        Go to course
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <img class="h-px-400 w-100 py-5 px-5" style="border-radius: 100px" src="https://upload.wikimedia.org/wikipedia/commons/9/91/JavaScript_screenshot.png" alt="Course image" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- List group Icons -->
                        <!--<p class="text-light fw-semibold">ALL COURSES</p>-->
                        <div class="row ">

                            <div class="demo-inline-spacing mt-3 col-md-4">
                                <h5 class="text-light fw-semibold">ALL COURSES</h5>
                                <ul class="list-group card">

                                    <li class="list-group-item d-flex align-items-center" >
                                        <i class="bx bx-tv me-2"></i>
                                        Soufflé pastry pie ice
                                    </li>
                                    <li class="list-group-item d-flex align-items-center">
                                        <i class="bx bx-bell me-2"></i>
                                        Bear claw cake biscuit
                                    </li>
                                    <li class="list-group-item d-flex align-items-center">
                                        <i class="bx bx-support me-2"></i>
                                        Tart tiramisu cake
                                    </li>
                                    <li class="list-group-item d-flex align-items-center">
                                        <i class="bx bx-purchase-tag-alt me-2"></i>
                                        Bonbon toffee muffin
                                    </li>
                                    <li class="list-group-item d-flex align-items-center">
                                        <i class="bx bx-closet me-2"></i>
                                        Dragée tootsie roll
                                    </li>
                                </ul>
                            </div>

                            <div class="demo-inline-spacing mt-3 col-md-4">
                                <h5 class="text-light fw-semibold">ALL COURSES</h5>
                                <ul class="list-group card">
                                    <li class="list-group-item d-flex align-items-center" >
                                        <i class="bx bx-tv me-2"></i>
                                        Soufflé pastry pie ice
                                    </li>
                                    <li class="list-group-item d-flex align-items-center">
                                        <i class="bx bx-bell me-2"></i>
                                        Bear claw cake biscuit
                                    </li>
                                    <li class="list-group-item d-flex align-items-center">
                                        <i class="bx bx-support me-2"></i>
                                        Tart tiramisu cake
                                    </li>
                                    <li class="list-group-item d-flex align-items-center">
                                        <i class="bx bx-purchase-tag-alt me-2"></i>
                                        Bonbon toffee muffin
                                    </li>
                                    <li class="list-group-item d-flex align-items-center">
                                        <i class="bx bx-closet me-2"></i>
                                        Dragée tootsie roll
                                    </li>
                                </ul>
                            </div>
                            <div class="demo-inline-spacing mt-3 col-md-4">
                                <h5 class="text-light fw-semibold">ALL COURSES</h5>
                                <ul class="list-group card">
                                    <li class="list-group-item d-flex align-items-center" >
                                        <i class="bx bx-tv me-2"></i>
                                        Soufflé pastry pie ice
                                    </li>
                                    <li class="list-group-item d-flex align-items-center">
                                        <i class="bx bx-bell me-2"></i>
                                        Bear claw cake biscuit
                                    </li>
                                    <li class="list-group-item d-flex align-items-center">
                                        <i class="bx bx-support me-2"></i>
                                        Tart tiramisu cake
                                    </li>
                                    <li class="list-group-item d-flex align-items-center">
                                        <i class="bx bx-purchase-tag-alt me-2"></i>
                                        Bonbon toffee muffin
                                    </li>
                                    <li class="list-group-item d-flex align-items-center">
                                        <i class="bx bx-closet me-2"></i>
                                        Dragée tootsie roll
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--/ List group Icons -->
                </div>
                <!--End Info-course-->
            </div>
            <!-- / Content wrapper -->
            
        </div>
    </div>

</body>
</html>
